use Cro::HTTP::Log::File;
use Cro::HTTP::Server;
use Routes;
use Beaverapp;

my $beaverapp = Beaverapp.new;
my $application = routes($beaverapp);

my Cro::Service $http = Cro::HTTP::Server.new(
    http => <1.1>,
    host => %*ENV<BEAVERAPP_HOST> ||
        die("Missing BEAVERAPP_HOST in environment"),
    port => %*ENV<BEAVERAPP_PORT> ||
        die("Missing BEAVERAPP_PORT in environment"),
    tls => %(
        private-key-file => %*ENV<BEAVERAPP_TLS_KEY> ||
            %?RESOURCES<fake-tls/server-key.pem> || "resources/fake-tls/server-key.pem",
        certificate-file => %*ENV<BEAVERAPP_TLS_CERT> ||
            %?RESOURCES<fake-tls/server-crt.pem> || "resources/fake-tls/server-crt.pem",
    ),
    :$application,
    after => [
        Cro::HTTP::Log::File.new(logs => $*OUT, errors => $*ERR)
    ]
);
$http.start;
say "Listening at https://%*ENV<BEAVERAPP_HOST>:%*ENV<BEAVERAPP_PORT>";
react {
    whenever signal(SIGINT) {
        say "Shutting down...";
        $http.stop;
        done;
    }
}
