use Cro::HTTP::Router;
use Cro::HTTP::Session::InMemory;
use Cro::HTTP::Auth;
use Beaverapp;

class UserSession does Cro::HTTP::Auth {
    has $.username is rw;

    method logged-in() {
        defined $!username;
    }
}

my $routes = route {

    ####################################
    # USER LOGIN
    ####################################
    subset LoggedIn of UserSession where *.logged-in;

    get -> UserSession $s {
        content 'text/html', "Current user: {$s.logged-in ?? $s.username !! '-'}";
    }

    get -> LoggedIn $user, 'users-only' {
        content 'text/html', "Secret page just for *YOU*, $user.username()";
    }

    get -> 'login' {
        content 'text/html', q:to/HTML/;
            <form method="POST" action="/login">
              <div>
                Username: <input type="text" name="username" />
              </div>
              <div>
                Password: <input type="password" name="password" />
              </div>
              <input type="submit" value="Log In" />
            </form>
            HTML
    }

    post -> UserSession $user, 'login' {
        request-body -> (:$username, :$password, *%) {
            if valid-user-pass($username, $password) {
                $user.username = $username;
                redirect '/', :see-other;
            }
            else {
                content 'text/html', "Bad username/password";
            }
        }
    }

    sub valid-user-pass($username, $password) {
        # Call a database or similar here
        return $username eq 'user' && $password eq 'pwd';
    }

    ##################################
    #### APPLICATION
    ##################################
    get -> {
        static 'static/index.html'
    }
    get -> 'js', *@path {
        static 'static/js', @path
    }
    get -> LoggedIn $user, 'search', 'firma', Int :$firmanr! {
        content 'text/html', "<p>You are looking for firma $firmanr </p><p>and you are {$user.username}</p>"
    }
}

sub routes(Beaverapp $beaverapp) is export {
    route {
        # Apply middleware, then delegate to the routes.
        before Cro::HTTP::Session::InMemory[UserSession].new;
        delegate <*> => $routes;
    }
}
